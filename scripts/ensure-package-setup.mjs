#!/usr/bin/env node

import { processPackage } from '@3xpo/pkgmetatool';
import fs from 'fs';
import path from 'path';
import process from 'process';
import { execSync } from 'child_process';

const monorepoRoot = process.cwd();

const packages = execSync('nx exec -- pwd')
  .toString()
  .split('\n')
  .map(x => x.trim())
  .filter(x => x.length > 0 && fs.existsSync(x))
  .map(x => path.relative(monorepoRoot, x));

packages.forEach(pkg => {
  const pkgPath = path.join(monorepoRoot, pkg, 'package.json');
  const pkgJson = JSON.parse(fs.readFileSync(pkgPath, 'utf-8'));
  console.log(`Process package: ${pkg}`);
  fs.writeFileSync(
    pkgPath,
    JSON.stringify(
      processPackage(pkgJson, {
        path: pkg,
        license: 'AGPL-3.0-OR-LATER',
        author: 'Expo',
        repository: 'https://codeberg.org/Velox/Velox.git',
        bugs: {
          url: 'AUTO',
          email: 'velox.bugs@breadhub.cc',
        },
        ensureExports: true,
        fallbackTypings: true,
        sort: true,
      }),
      null,
      2,
    ) + '\n',
  );
});
