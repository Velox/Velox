<div align="center">

[![Velox Logo](https://codeberg.org/Velox/Velox/raw/branch/master/assets/icons/velox.png)](https://codeberg.org/Velox)

# Velox

Next-Generation Blazingly Fast Svelte Discord Client

[![Codeberg](https://img.shields.io/badge/Codeberg-Velox-724773.svg?style=flat-square)](https://codeberg.org/Velox/Velox)
[![License](https://img.shields.io/badge/License-AGPL-724773.svg?style=flat-square)](https://codeberg.org/Velox/Velox/src/branch/master/LICENSE)

</div>

## Notice

This is in a _very_ early alpha stage. Please only use this on alts for now.

Additionally, using this outside of bots does violate TOS lol.

## About

TBA

## Features

- TBA
- TBA
- TBA

## Licensing

This codebase, in its entirety (unless otherwise specified), is licensed under the [GNU Affero General Public License v3.0](https://www.gnu.org/licenses/agpl-3.0.html). For more information, please see the [LICENSE](https://codeberg.org/Velox/Velox/src/branch/master/LICENSE) file.

Machine Learning Disclaimer:

> **This codebase may not be used for machine learning or data mining purposes, unless the dataset & any models derived from them are also licensed under the GNU Affero General Public License and operates in compliance with the AGPL.** Consent is opt-in, not opt-out.<br/>
> Noncompliance with the AGPL will result in a DMCA takedown request. Please respect the license.
