/*!
 * Dual-licensed under the MIT License and the AGPL-3.0-OR-LATER.
 *
 * See the LICENSE file in the parent directory of this file for more details.
 *
 * Copyright (c) 2024 Expo.
 *
 * @license AGPL-3.0-OR-LATER OR MIT
 * @preserve
 */
void 0; // ensure the comment does not fall into the class' jsdoc

/**
 * The pure-JS Base64 Encoder/Decoder. It's strongly suggested to use the {@link atob} and {@link btoa} exports to achieve additional performance obtained by the native environment when it's available.
 */
export class Base64 {
  /**
   * The character used to pad the output to a multiple of 4 characters.<br/>
   * Set to a blank string if you don't desire padding.
   */
  public PAD_CHAR = '=';

  /**
   * The alphabet used in the encoding/decoding process.<br/>
   * If modified, ensure that the new alphabet is 64 characters long and doesn't contain the padding character.<br/>
   * The default alphabet is the standard base64 alphabet. Do not modify unless you know what you're doing.
   */
  public ALPHA =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

  /**
   * Converts a binary/text value into a base64-encoded pure-ascii value.
   * This function will not use platform-specific functions, even if they are available.
   * If you desire said additional performance, see the {@link btoa} export.
   */
  public btoa(input: string) {
    let output = '';
    let chr1: number, chr2: number, chr3: number;
    let enc1: number, enc2: number, enc3: number, enc4: number;
    let i = 0;

    while (i < input.length) {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) enc3 = enc4 = 64;
      else if (isNaN(chr3)) enc4 = 64;

      output =
        output +
        this.ALPHA.charAt(enc1) +
        this.ALPHA.charAt(enc2) +
        this.ALPHA.charAt(enc3) +
        this.ALPHA.charAt(enc4);
    }

    let padding = 3 - ((output.length + 3) % 4);
    if (padding === 3) output += this.PAD_CHAR;
    else if (padding) for (let i = 0; i < padding; i++) output += this.PAD_CHAR;

    return output;
  }

  /**
   * Converts a base64-encoded pure-ascii value into a binary/text value.
   *
   * This function will not use platform-specific functions, even if they are available.
   * If you desire said additional performance, see the {@link atob} export.
   */
  public atob(input: string) {
    let output = '';
    let enc1: number, enc2: number, enc3: number, enc4: number;
    let i = 0;

    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, '');

    while (i < input.length) {
      enc1 = this.ALPHA.indexOf(input.charAt(i++));
      enc2 = this.ALPHA.indexOf(input.charAt(i++));
      enc3 = this.ALPHA.indexOf(input.charAt(i++));
      enc4 = this.ALPHA.indexOf(input.charAt(i++));

      output += String.fromCharCode((enc1 << 2) | (enc2 >> 4));
      if (enc3 !== 64)
        output += String.fromCharCode(((enc2 & 15) << 4) | (enc3 >> 2));
      if (enc4 !== 64) output += String.fromCharCode(((enc3 & 3) << 6) | enc4);
    }

    return output;
  }
}

/** An instance of {@link Base64 the Base64 Class}. Used as a fallback in {@link atob} and {@link btoa} exports when the native options aren't available. */
export const base64 = new Base64();

const isBufferAvailable = typeof Buffer === 'function';

/** In modern node, we should use Buffer.from to construct a buffer */
const isModernNode =
  isBufferAvailable && Buffer.from && Buffer.from !== Uint8Array.from;

/** In legacy node, we should use new Buffer to construct a buffer */
const isLegacyNode = isBufferAvailable && !isModernNode;

/** In the browser, we should use the global atob/btoa functions */
const isBrowserWithAtobAndBtoa = // we only run this when we're in a browser-like environment as node's atob/btoa are deprecated (https://nodejs.org/api/all.html#all_buffer_bufferatobdata)
  typeof globalThis !== 'undefined' &&
  typeof (globalThis as any)['window'] !== 'undefined' &&
  typeof globalThis.atob === 'function' &&
  typeof globalThis.btoa === 'function';

/** Binary/Decoded to Base64/Ascii */
export type BTOA = (data: string) => string;

/** Base64/Ascii to Binary/Decoded */
export type ATOB = (data: string) => string;

/** Platform-Optimized `Binary/Decoded` to `Base64/Ascii` implementation */
export const btoa: BTOA = isModernNode
  ? (input: string) => Buffer.from(input, 'utf-8').toString('base64')
  : isLegacyNode
    ? (input: string) => new Buffer(input, 'utf-8').toString('base64')
    : isBrowserWithAtobAndBtoa
      ? globalThis.btoa
      : base64.btoa.bind(base64);

/** Platform-Optimized `Base64/Ascii` to `Binary/Decoded` implementation */
export const atob: ATOB = isModernNode
  ? (input: string) => Buffer.from(input, 'base64').toString('utf-8')
  : isLegacyNode
    ? (input: string) => new Buffer(input, 'base64').toString('utf-8')
    : isBrowserWithAtobAndBtoa
      ? globalThis.atob
      : base64.atob.bind(base64);
