<div align="center">

[![Velox Logo](https://codeberg.org/Velox/Velox/raw/branch/master/assets/icons/velox.png)](https://codeberg.org/Velox)

# @veloxapp/abab

Reliable, cross-environment ATOB and BTOA conversion. Inspired by the deprecated [abab](https://www.npmjs.com/package/abab) library.

[![Codeberg](https://img.shields.io/badge/Codeberg-Velox-724773.svg?style=flat-square)](https://codeberg.org/Velox/Velox)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Velox/Velox/src/branch/master/LICENSE)

</div>

## Installation

```bash
pnpm i @veloxapp/abab
```

## Usage

```ts
import { atob, btoa } from '@veloxapp/abab'; // the atob/btoa exports will use per-platform implementations, falling back to the pure implementation

console.log(atob('SGVsbG8gV29ybGQ=')); // Hello World
console.log(btoa('Hello World')); // SGVsbG8gV29ybGQ=
```

### Using Pure Implementation

If you want to _always_ use the pure-js implementation, rather than opting for the (likely more performant) native platform implementations where possible, you can import it directly:

```ts
import { Base64 } from '@veloxapp/abab';

const base64 = new Base64(); // you can also use the lowercase `base64` export, which is what the `atob` and `btoa` exports use when the native implementations are unavailable
```

## License

This package, unlike the other components of Velox, is dual-licensed under the [MIT License](https://opensource.org/licenses/MIT). For more information, please see the [LICENSE](https://codeberg.org/Velox/Velox/src/branch/master/packages/@veloxapp/abab/LICENSE) file.
