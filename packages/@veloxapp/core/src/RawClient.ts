import { atob } from '@veloxapp/abab';
import { EventEmitter, type AnyFunc } from '@3xpo/events';

export type RawClientEvents = {
  'token-change': (newToken: string, oldToken: string) => void;
};
export type MergeEvents<A, B> = {
  [Key in keyof A]: Key extends keyof B ? B[Key] : A[Key];
} & {
  [Key in keyof B]: B[Key];
};

/** The core client class; wraps fetch() and stores the token, and provides minimal utilities */
export default class RawClient<
  T extends Record<string, AnyFunc> = {},
> extends EventEmitter<MergeEvents<RawClientEvents, T>> {
  public constructor(protected _token: string) {
    if (_token === undefined)
      throw new Error('Must pass token to client constructor');
    if (_token === null) throw new Error('Cannot pass null token');
    if (_token === '') throw new Error('Cannot pass empty token');
    super();
  }

  // #region Token
  public get token() {
    if (this._token === '') throw new Error('Token is empty???');
    return this._token;
  }
  public getToken() {
    return this.token;
  }
  public set token(token: string) {
    if (token === '') throw new Error('Cannot set token to an empty string');
    if (token === null) throw new Error('Cannot set token to null');

    this.rcEmit('token-change', token, this._token);
    this._token = token;
  }
  public setToken(newToken: string) {
    this.token = newToken;
    return this;
  }
  // #endregion

  // #region Utilities
  public get userId() {
    return atob(this.token.substring(0, this.token.indexOf('.')));
  }
  protected _rawClientEmit<K extends keyof RawClientEvents & (string | symbol)>(
    event: K,
    ...args: Parameters<RawClientEvents[K]>
  ) {
    return this.emit(event, ...args);
  }
  private rcEmit = this._rawClientEmit;
  // #endregion

  // #region HTTP Helpers
  public async fetch(url: string, init?: RequestInit) {
    if (init === undefined) init = {};
    if (init.headers === undefined) init.headers = {};
    if (init.headers instanceof Headers)
      init.headers.set('Authorization', this.token);
    else if (Array.isArray(init.headers))
      init.headers.push(['Authorization', this.token]);
    else init.headers['Authorization'] = this.token;
    return fetch(url, init);
  }
  public async get(url: string, init?: RequestInit) {
    if (init === undefined) init = {};
    init.method = 'GET';
    return this.fetch(url, init);
  }
  public async post(
    url: string,
    // we demand the init as a POST without a body is almost certainly a mistake - note that an empty object of `{}` will still be accepted, just incase the user explicitly wants to send an empty body
    init: RequestInit & {
      method?: never; // ensure the user doesn't override the method
    },
  ) {
    if (init === undefined) init = {};
    init.method = 'POST' as any;
    return this.fetch(url, init);
  }
  // #endregion
}
