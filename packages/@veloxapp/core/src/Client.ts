import RawClient from './RawClient';
import WebSocket from 'isomorphic-ws';
import type WS from 'ws';
import consola from 'consola';
import { GatewayRecieveEvent } from '@veloxapp/discord-types';

export class Client extends RawClient<{
  /** Is emitted once the socket is connected, however prior to auth */
  socket_connected: () => void;
  /** Is emitted once the client has fully initialized */
  ready: (type: 'initial_connect' | 'reconnect') => void;
  /** Is emitted once the socket is disconnected */
  disconnected: () => void;
  /** Is emitted once we automatically try to reconnect */
  reconnecting: () => void;
  /** Is emitted once we successfully reconnect */
  reconnected: () => void;
  /** An individual socket event */
  socket_data: (event: GatewayRecieveEvent) => void;
}> {
  public constructor(protected _token: string) {
    super(_token);

    // #region Socket Events

    // Once we've connected, we should indicate that the next connection is no longer the initial connection
    this.on('ready', () => {
      if (this.isNextReadyInitial) this.isNextReadyInitial = false;
    });

    // When the token changes, we should disconnect the socket & create a new one
    this.on('token-change', newToken => {
      if (this._socket) {
        this._socket.close();
        this._socket = null;
      }
    });
    this.connect();

    // #endregion
  }

  // #region Internal Stuff
  protected isNextReadyInitial = true;
  private _socket: WS | null = null;
  public get socket() {
    return this._socket;
  }
  // #endregion

  // #region Low-Level Socket
  protected socketURL =
    'wss://gateway.discord.gg/?encoding=json&v=9&compress=zlib-stream';
  public get socketUrl() {
    return this.socketURL;
  }
  public set socketUrl(url: string) {
    this.socketURL = url;
    this.connect(true);
  }
  public get isSocketCompressed() {
    return this.socketURL.includes('compress=zlib-stream');
  }
  protected login() {}
  protected connect(forceReconnect = false) {
    if (this.socket && !forceReconnect) return;
    else if (this.socket) this.socket.close();

    this._socket = new WebSocket(this.socketURL);
    if (!this.socket) throw new Error('sanity check failed');
    this.socket.onopen = () => {
      this.emit('socket_connected');
    };
    this.socket.onclose = () => {
      this.emit('disconnected');
    };
    this.socket.onerror = e => {
      consola.error('Socket Error', e);
    };
    this.socket.onmessage = message => {
      const data = JSON.parse(message.data.toString());
      this.emit('socket_data', data);
    };

    return new Promise<void>((resolve, reject) => {
      this.once('socket_connected', () => {
        this.emit(
          'ready',
          this.isNextReadyInitial ? 'initial_connect' : 'reconnect',
        );
        resolve();
      });
      this.once('disconnected', () => {
        this.emit('reconnecting');
        this.connect(true);
      });
    });
  }
  // #endregion
}
