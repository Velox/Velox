<div align="center">

[![Velox Logo](https://codeberg.org/Velox/Velox/raw/branch/master/assets/icons/velox.png)](https://codeberg.org/Velox)

# @veloxapp/core

Core Discord API/Gateway Abstraction.

[![Codeberg](https://img.shields.io/badge/Codeberg-Velox-724773.svg?style=flat-square)](https://codeberg.org/Velox/Velox)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Velox/Velox/src/branch/master/LICENSE)

</div>

## Note

This does not guarantee full coverage of the Discord API, but aims to provide a solid foundation for building Discord bots and applications.

This will attempt topass types as they are recieved from the Discord API, and are not significantly further abstracted. See [@veloxapp/api](TODO:LINK) for a more "stable" abstraction over this package.

## Installation

```bash
pnpm i @veloxapp/core
```

## Usage

TODO: Add Usage Information
