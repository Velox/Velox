import { z } from 'zod';
import { UpdatePresence } from '../Misc/Presence';

/**
 * The IdentifyConnection type derived from Discord Documentation
 * @see OfficialClientIdentifyConnection for additional fields present in a real client
 */
export const DocumentedIdentifyConnection = z.object({
  /** Your Operating System */
  os: z.string().default('Linux'),
  /** "Your Library Name" (Your Browser if connecting as regular client; `"Chrome"`/`"Firefox"`) */
  browser: z.string().default('Firefox'),
  /** "Your Library Name" (Blank in regular clients lol) */
  device: z.string().default(''),
});

/** The IdentifyConnection type used by a real client */
export const OfficialClientIdentifyConnection =
  DocumentedIdentifyConnection.and(
    z.object({
      /** The browser's User Agent */
      browser_user_agent: z
        .string()
        .default(
          `Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36`,
        ),
      /** The browser's version */
      browser_version: z.string().default('122.0.0.0'),
      /** The Discord Client's build number */
      client_build_number: z.number().default(277643),
      /** Unknown, I can only get `null` */
      client_event_source: z.null(),
      /** The OS' version, on linux this si a blank string */
      client_os: z.string().default(''),
      /** Unknown */
      referrer: z.string().default(''),
      /** Unknown */
      referrer_current: z.string().default(''),
      /** Unknown */
      referring_domain: z.string().default(''),
      /** Unknown */
      referring_domain_current: z.string().default(''),
      /** The release channel the client is on */
      release_channel: z.string().default('stable'),
      /** The system's locale */
      system_locale: z.string().default('en-US'),
    }),
  );

/**
 * The Identify type derived from Discord Documentation, use this in bots
 */
export const DocumentedIdentify = z.object({
  /** Authentication token */
  token: z.string(),
  /** {@link https://discord.com/developers/docs/topics/gateway-events#identify-identify-connection-properties Connection properties} */
  properties: DocumentedIdentifyConnection,
  /** Whether this connection supports compression of packets - false in real clients even when recieving compressed packets */
  compress: z.boolean().default(false).optional(),
  /** Value between 50 and 250, total number of members where the gateway will stop sending offline members in the guild member list */
  large_threshold: z.number().int().min(50).max(250).default(50).optional(),
  /** Used for {@link https://discord.com/developers/docs/topics/gateway#sharding Guild Sharding} */
  shard: z.array(z.number()).length(2).optional(),
  /** Presence structure for initial presence information */
  presence: UpdatePresence.optional(),
  /** A list of {@link https://discord.com/developers/docs/topics/gateway#gateway-intents intents} you wish to receive */
  intents: z.number().int().optional(),
});

/**
 * The Identify type used by a real client, use this when connecting as a real client
 */
export const Identify = DocumentedIdentify.and(
  z.object({
    /** Connection properties */
    properties: OfficialClientIdentifyConnection,
    /** Unused by real clients */
    shard: z.never().optional(),
    /** Unused by real clients */
    intents: z.never().optional(),
    /** Unknown */
    capabilities: z.number().int().default(16381),
    /** The client_state object */
    client_state: z.object({
      /** Unknown */
      guild_versions: z.object({}),
    }),
  }),
);
