import { z } from 'zod';

export const VoiceStateUpdate = z
  .union([
    z.object({
      channel_id: z.string(),
      guild_id: z.string(),
    }),
    z.object({
      channel_id: z.null(),
      guild_id: z.null(),
    }),
  ])
  .and(
    z.object({
      flags: z.number().default(0),
      self_mute: z.boolean().default(false),
      self_deaf: z.boolean().default(false),
      self_video: z.boolean().default(false),
    }),
  );
