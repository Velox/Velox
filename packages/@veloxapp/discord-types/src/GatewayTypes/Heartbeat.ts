import { z } from 'zod';

/** Used to maintain an active gateway connection. Must be sent every heartbeat_interval milliseconds after the {@link https://discord.com/developers/docs/topics/gateway-events#hello Opcode 10 Hello} payload is received. The inner `d` key (this value) is the last sequence number—`s`—received by the client. If you have not yet received one, send `null`. - {@link https://discord.com/developers/docs/topics/gateway-events#heartbeat} */
export const Heartbeat = z.number().int();
