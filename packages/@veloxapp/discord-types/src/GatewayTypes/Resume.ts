import { z } from 'zod';

export const Resume = z.object({
  /** Session token */
  token: z.string(),
  /** Session id */
  session_id: z.string(),
  /** Last sequence number received */
  seq: z.number().int(),
});
