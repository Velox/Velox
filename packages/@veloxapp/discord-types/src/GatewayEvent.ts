import { Opcode, RecieveOpcodes, SendOpcodes } from './Opcodes';
import { GatewayRecieveDataTable, GatewaySendDataTable } from './GatewayData';

/** The array describing each individual opcode's data, falling back to any for untyped opcodes */
export type EventDataByOpcodeArray = {
  [Key in Opcode]: Key extends keyof GatewayRecieveDataTable
    ? GatewayRecieveDataTable[Key]
    : any;
};
/** Shorthand for indexing {@link EventDataByOpcodeArray} */
export type EventDataByOpcode<Op extends Opcode> = EventDataByOpcodeArray[Op];

/** The type of an incoming gateway event */
export type GatewayRecieveEvent<Op extends RecieveOpcodes = RecieveOpcodes> = {
  /** {@link https://discord.com/developers/docs/topics/opcodes-and-status-codes#gateway-gateway-opcodes Event Opcode} */
  op: Op;
  /** Event Data */
  d?: EventDataByOpcode<Op>;
  /** Sequence number of event used for {@link https://discord.com/developers/docs/topics/gateway#resuming resuming sessions} and {@link https://discord.com/developers/docs/topics/gateway#sending-heartbeats heartbeating} */
  s?: Op extends 0 ? number : null;
  /** Event Name */
  t?: Op extends 0 ? string : null;
};

/** The type of an outgoing gateway event */
export type GatewaySendEvent<Op extends SendOpcodes = SendOpcodes> = {
  /** {@link https://discord.com/developers/docs/topics/opcodes-and-status-codes#gateway-gateway-opcodes Event Opcode} */
  op: Op;
  /** Event Data */
  d: Op extends keyof GatewaySendDataTable ? GatewaySendDataTable[Op] : any;
};
