export enum Opcode {
  /** An event was dispatched (Client Action: Recieve) */
  Dispatch = 0,
  /** Fired periodically by the client to keep the connection alive (Client Action: Send, Recieve) */
  Heartbeat = 1,
  /** Starts a new session during the initial handshake (Client Action: Send) */
  Identify = 2,
  /** Update the client's presence (Client Action: Send) */
  PresenceUpdate = 3,
  /** Used to join/leave or move between voice channels (Client Action: Send) */
  VoiceStateUpdate = 4,
  /** Undocumented: Voice Server Ping */
  VoiceServerPing = 5,
  /** Resume a previous session that was disconnected (Client Action: Send) */
  Resume = 6,
  /** You should attempt to reconnect and resume immediately (Client Action: Recieve) */
  Reconnect = 7,
  /** Request information about offline guild members in a large guild (Client Action: Send) */
  RequestGuildMembers = 8,
  /** The session has been invalidated. You should reconnect and identify/resume accordingly (Client Action: Recieve) */
  InvalidSession = 9,
  /** Sent immediately after connecting, contains the `heartbeat_interval` to use (Client Action: Recieve) */
  Hello = 10,
  /** Sent in response to receiving a heartbeat to acknowledge that it has been received (Client Action: Recieve) */
  HeartbeatACK = 11,
  /** Undocumented: Call Connect */
  CallConnect = 13,
  /** Undocumented: Guild Subscriptions */
  GuildSubscriptions = 14,
  /** Undocumented: Lobby Connect, afaik fires when someone joins the VC */
  LobbyConnect = 15,
  /** Undocumented: Lobby Disconnect, afaik fires when someone leaves the VC */
  LobbyDisconnect = 16,
  /** Undocumented: Lobby Voice States Update */
  LobbyVoiceStatesUpdate = 17,
  /** Undocumented: Stream Create, fired when a stream starts */
  StreamCreate = 18,
  /** Undocumented: Stream Delete, fired when a stream ends */
  StreamDelete = 19,
  /** Undocumented: Stream Watch, fired when you start watching a stream */
  StreamWatch = 20,
  /** Undocumented: Stream Ping */
  StreamPing = 21,
  /** Undocumented: Stream Set Paused, I have no clue what this is for */
  StreamSetPaused = 22,
  /** Undocumented: Request Guild Application Commands */
  RequestGuildApplicationCommands = 24,
  /** Undocumented: Embedded Activity Launch */
  EmbeddedActivityLaunch = 25,
  /** Undocumented: Embedded Activity Close */
  EmbeddedActivityClose = 26,
  /** Undocumented: Embedded Activity Update */
  EmbeddedActivityUpdate = 27,
  /** Undocumented: Request Forum Unreads */
  RequestForumUnreads = 28,
  /** Undocumented: Remote Command */
  RemoteCommand = 29,
  /** Undocumented: Get Deleted Entity IDs Not Matching Hash */
  GetDeletedEntityIDsNotMatchingHash = 30,
  /** Undocumented: Request Soundboard Sounds */
  RequestSoundboardSounds = 31,
  /** Undocumented: Speed Test Create */
  SpeedTestCreate = 32,
  /** Undocumented: Speed Test Delete */
  SpeedTestDelete = 33,
  /** Undocumented: Request Last Messages */
  RequestLastMessages = 34,
  /** Undocumented: Search Recent Members */
  SearchRecentMembers = 35,
  /** Undocumented: Request Channel Statuses */
  RequestChannelStatuses = 36,
  /** Undocumented: Guild Subscriptions Bulk */
  GuildSubscriptionsBulk = 37,
}

/** Opcodes with client action: send */
export type SendOpcodes =
  | Opcode.Heartbeat
  | Opcode.Identify
  | Opcode.PresenceUpdate
  | Opcode.VoiceStateUpdate
  | Opcode.Resume
  | Opcode.RequestGuildMembers;
/** Opcodes with client action: recieve */
export type RecieveOpcodes =
  | Opcode.Dispatch
  | Opcode.Heartbeat
  | Opcode.Reconnect
  | Opcode.InvalidSession
  | Opcode.Hello
  | Opcode.HeartbeatACK;
