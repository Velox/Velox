/** Consult {@link https://discord.com/developers/docs/topics/opcodes-and-status-codes#gateway-gateway-close-event-codes Discord Documentation} for information on if you should reconnect or not when getting one of these! */
export enum CloseCodes {
  /** We're not sure what went wrong. Try reconnecting? */
  UnknownError = 4000,
  /** You sent an invalid {@link https://discord.com/developers/docs/topics/opcodes-and-status-codes#gateway-gateway-opcodes Gateway opcode} or an invalid payload for an opcode. Don't do that! */
  UnknownOpcode = 4001,
  /** You sent an invalid {@link https://discord.com/developers/docs/topics/gateway#sending-events payload} to us. Don't do that! */
  DecodeError = 4002,
  /** You sent us a payload prior to {@link https://discord.com/developers/docs/topics/gateway#identifying identifying} */
  NotAuthenticated = 4003,
  /** The account token sent with your {@link https://discord.com/developers/docs/topics/gateway-events#identify identify payload} is incorrect */
  AuthenticationFailed = 4004,
  /** You sent more than one {@link https://discord.com/developers/docs/topics/gateway-events#identify identify payload}. Don't do that! */
  AlreadyAuthenticated = 4005,
  /** The sequence sent when {@link https://discord.com/developers/docs/topics/gateway-events#resume resuming} the session was invalid. Reconnect and start a new session */
  InvalidSeq = 4007,
  /** Woah nelly! You're sending payloads to us too quickly. Slow it down! You will be disconnected on receiving this */
  RateLimited = 4008,
  /** Your session timed out. Reconnect and start a new one */
  SessionTimedOut = 4009,
  /** You sent us an invalid {@link https://discord.com/developers/docs/topics/gateway#sharding shard when identifying} */
  InvalidShard = 4010,
  /** The session would have handled too many guilds - you are required to {@link https://discord.com/developers/docs/topics/gateway#sharding shard} your connection in order to connect */
  ShardingRequired = 4011,
  /** You sent an invalid version for the gateway */
  InvalidAPIVersion = 4012,
  /** You sent an invalid intent for a {@link https://discord.com/developers/docs/topics/gateway#gateway-intents Gateway Intent}. You may have incorrectly calculated the bitwise value */
  InvalidIntents = 4013,
  /** You sent a disallowed intent for a {@link https://discord.com/developers/docs/topics/gateway#gateway-intents Gateway Intent}. You may have tried to specify an intent that you {@link https://discord.com/developers/docs/topics/gateway#privileged-intents have not enabled or are not approved for} */
  DisallowedIntents = 4014,
}
