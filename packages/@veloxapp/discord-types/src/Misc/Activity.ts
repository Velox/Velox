// This file describes all of the `Activity *` types described in https://discord.com/developers/docs/topics/gateway-events#activity-object

import { z } from 'zod';

/** {@link https://discord.com/developers/docs/topics/gateway-events#activity-object-activity-types} */
export enum AcitivtyType {
  /** Playing {name} */
  Game = 0,
  /** Streaming {name} */
  Streaming = 1,
  /** Listening to {name} */
  Listening = 2,
  /** Watching {name} */
  Watching = 3,
  /** {emoji} {state} */
  Custom = 4,
  /** Competing in {name} */
  Competing = 5,
}

/** {@link https://discord.com/developers/docs/topics/gateway-events#activity-object-activity-flags} */
export enum ActivityFlags {
  INSTANCE = 1 << 0,
  JOIN = 1 << 1,
  SPECTATE = 1 << 2,
  JOIN_REQUEST = 1 << 3,
  SYNC = 1 << 4,
  PLAY = 1 << 5,
  PARTY_PRIVACY_FRIENDS = 1 << 6,
  PARTY_PRIVACY_VOICE_CHANNEL = 1 << 7,
  EMBEDDED = 1 << 8,
}

/** {@link https://discord.com/developers/docs/topics/gateway-events#activity-object-activity-timestamps} */
export const ActivitiesTimestamps = z.object({
  start: z.number().int().optional(),
  end: z.number().int().optional(),
});
export type ActivitiesTimestamps = z.infer<typeof ActivitiesTimestamps>;

/** {@link https://discord.com/developers/docs/topics/gateway-events#activity-object-activity-emoji} */
export const ActivityEmoji = z.object({
  /** The name of the emoji */
  name: z.string(),
  /** The id of the emoji */
  id: z.string().optional(),
  /** Whether this emoji is animated */
  animated: z.boolean().optional(),
});
export type ActivityEmoji = z.infer<typeof ActivityEmoji>;

/** {@link https://discord.com/developers/docs/topics/gateway-events#activity-object-activity-party} */
export const ActivityParty = z.object({
  /** The id of the party */
  id: z.string().optional(),
  /** Used to show the party's current and maximum size - [current_size,max_size] */
  size: z.array(z.number()).length(2).optional(),
});
export type ActivityParty = z.infer<typeof ActivityParty>;

/** {@link https://discord.com/developers/docs/topics/gateway-events#activity-object-activity-assets} */
export const ActivityAssets = z.object({
  /** The id for a large asset of the activity, usually a snowflake -  {@link https://discord.com/developers/docs/topics/gateway-events#activity-object-activity-asset-image Activity Asset Image} */
  large_image: z.string().optional(),
  /** Text displayed when hovering over the large image of the activity */
  large_text: z.string().optional(),
  /** The id for a small asset of the activity, usually a snowflake -  {@link https://discord.com/developers/docs/topics/gateway-events#activity-object-activity-asset-image Activity Asset Image} */
  small_image: z.string().optional(),
  /** Text displayed when hovering over the small image of the activity */
  small_text: z.string().optional(),
});
export type ActivityAssets = z.infer<typeof ActivityAssets>;

/** {@link https://discord.com/developers/docs/topics/gateway-events#activity-object-activity-secrets} */
export const ActivitySecrets = z.object({
  /** The secret for joining a party */
  join: z.string().optional(),
  /** The secret for spectating a game */
  spectate: z.string().optional(),
  /** The secret for a specific instanced match */
  match: z.string().optional(),
});
export type ActivitySecrets = z.infer<typeof ActivitySecrets>;

/** {@link https://discord.com/developers/docs/topics/gateway-events#activity-object-activity-buttons} */
export const ActivityButton = z.object({
  label: z.string().min(1).max(32),
  url: z.string().url().min(1).max(512),
});
export type ActivityButton = z.infer<typeof ActivityButton>;

/** {@link https://discord.com/developers/docs/topics/gateway-events#activity-object-activity-structure} - Bot users are only able to set `name`, `state`, `type`, and `url`. */
export const Activity = z.object({
  /** The activity's name */
  name: z.string(),
  /** Activity type */
  type: z.nativeEnum(AcitivtyType),
  /** Stream url, is validated when type is {@link AcitivtyType.Streaming Streaming} */
  url: z.string().url().optional(),
  /** Unix timestamp (in unix millis) of when the activity was added to the user's session */
  created_at: z.number().int(),
  /** Unix timestamps for start and/or end of the game */
  timestamps: ActivitiesTimestamps.optional(),
  /** Application id for the game */
  application_id: z.string().optional(),
  /** What the player is currently doing */
  details: z.string().optional(),
  /** The user's current party status */
  state: z.string().optional(),
  /** The emoji used for a custom status */
  emoji: ActivityEmoji.optional(),
  /** Information for the current party of the player */
  party: ActivityParty.optional(),
  /** Images for the presence and their hover texts */
  assets: ActivityAssets.optional(),
  /** Secrets for Rich Presence joining and spectating */
  secrets: ActivitySecrets.optional(),
  /** Whether or not the activity is an instanced game session */
  instance: z.boolean().optional(),
  /** {@link ActivityFlags Activity flags} `OR`d together, describes what the payload includes */
  flags: z.number().int().optional(),
  /** Custom buttons shown in the Rich Presence (max 2) */
  buttons: z.array(ActivityButton).max(2).optional(),
});
export type Activity = z.infer<typeof Activity>;
