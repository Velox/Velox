export enum Status {
  /** Online */
  ONLINE = 'online',
  /** Do Not Disturb */
  DND = 'dnd',
  /** AFK */
  IDLE = 'idle',
  /** Invisible and shown as offline */
  INVISIBLE = 'invisible',
  /** Offline */
  OFFLINE = 'offline',
}
