import { z } from 'zod';
import { Activity } from './Activity';
import { Status } from './Status';

export const UpdatePresence = z.object({
  /** Unix time (in milliseconds) of when the client went idle, or null if the client is not idle */
  since: z.number().int().optional(),
  /** User's activities */
  activities: z.array(Activity),
  /** User's new {@link https://discord.com/developers/docs/topics/gateway-events#update-presence-status-types status} */
  status: z.nativeEnum(Status),
  /** Whether or not the client is afk */
  afk: z.boolean().default(false),
});
