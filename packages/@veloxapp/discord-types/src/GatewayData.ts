import { z } from 'zod';
import { Opcode } from './Opcodes';
import * as GatewayTypes from './GatewayTypes/index';

export type GatewayRecieveDataTable = {
  /** https://discord.com/developers/docs/topics/gateway-events#hello */
  [Opcode.Hello]: {
    /** Interval (in milliseconds) an app should heartbeat with */
    heartbeat_interval: number;
  };
};

export const GatewaySendDataTable = z.object({
  [Opcode.Identify]: GatewayTypes.Identify.DocumentedIdentify.or(
    GatewayTypes.Identify.Identify,
  ),
  [Opcode.Resume]: GatewayTypes.Resume.Resume,
  [Opcode.Heartbeat]: GatewayTypes.Heartbeat.Heartbeat,
  [Opcode.VoiceStateUpdate]: GatewayTypes.VoiceStateUpdate.VoiceStateUpdate,
});
export type GatewaySendDataTable = z.infer<typeof GatewaySendDataTable>;

export * as Identify from './GatewayTypes/Identify';
