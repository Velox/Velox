<div align="center">

[![Velox Logo](https://codeberg.org/Velox/Velox/raw/branch/master/assets/icons/velox.png)](https://codeberg.org/Velox)

# @veloxapp/discord-types

Discord Gateway Enums & general Gateway types.

[![Codeberg](https://img.shields.io/badge/Codeberg-Velox-724773.svg?style=flat-square)](https://codeberg.org/Velox/Velox)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Velox/Velox/src/branch/master/LICENSE)

</div>

## Installation

```bash
pnpm i @veloxapp/discord-types
```

## Usage

TODO: Add Usage Information

## Relevant Discord Documentation

[Gateway](https://discord.com/developers/docs/topics/gateway) - [Gateway Events](https://discord.com/developers/docs/topics/gateway-events) - [Opcodes and Status Codes](https://discord.com/developers/docs/topics/opcodes-and-status-codes)
